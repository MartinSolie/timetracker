package ru.startandroid.timetracker.db;

/**
 * Created by martin on 6/1/16.
 */

import android.provider.BaseColumns;

public class TaskContract {
    public static final String DB_NAME = "ru.startandroid.todolist.db";
    public static final int DB_VERSION = 1;

    public class TaskEntry implements BaseColumns{
        public static final String TABLE = "tasks";

        public static final String COL_TASK_TITLE = "title";
        public static final String COL_TASK_TIME = "time";
    }
}
