package ru.startandroid.timetracker;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by martin on 6/1/16.
 */
public class TaskList {
    private ArrayList<Task> tasks;

    public TaskList(){
        tasks = new ArrayList<>();
    }

    public TaskList(ArrayList<Task> tasks){
        this.tasks = new ArrayList<>(tasks);
    }

    public ArrayList<Task> getList(){
        return tasks;
    }

    public int getIndexByName(String name){
        for (int i = 0; i < tasks.size(); i++){
            if (tasks.get(i).name.equalsIgnoreCase(name)){
                return i;
            }
        }

        return -1;
    }

    public Task getTaskByName(String name){
        for (Task task : tasks){
            if (task.name.equalsIgnoreCase(name)){
                return task;
            }
        }
        return null;
    }

    public void removeByName(String name){
        int index = getIndexByName(name);
        if (index != -1) {
            tasks.remove(index);
        }
    }

    public void updateTimeByName(String name, long time){
        for (Task task : tasks){
            if (task.name.equalsIgnoreCase(name)){
                task.timeSpent = time;
                break;
            }
        }
    }
}
