package ru.startandroid.timetracker;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by martin on 6/1/16.
 */
public class Counter extends Service {
    final String TAG = "Service";

    private String name;
    private long time;
    private PendingIntent pi;

    private ExecutorService es;
    private Worker worker;

    public void onCreate(){
        super.onCreate();

        es = Executors.newScheduledThreadPool(1);
        worker = new Worker();
    }

    public int onStartCommand(Intent intent, int flags, int startId){

        if (intent != null && startId == 1) {
            time = intent.getLongExtra("time", 0);
            name = intent.getStringExtra("name");
            pi = intent.getParcelableExtra("pendingIntent");

            es.execute(worker);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy(){
        super.onDestroy();

        worker.isWorking = false;

        sendMessage(MainActivity.STATUS_FINISHED);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void sendMessage(int messageStatus){
        if (pi.getCreatorUserHandle() == null) return;

        Intent intent = new Intent().putExtra("time", time).putExtra("name", name);
        try {
            pi.send(Counter.this, messageStatus, intent);

        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

    private class Worker implements Runnable{
        public boolean isWorking = false;

        @Override
        public void run() {
            isWorking = true;

            while(isWorking){
                try {
                    time++;
                    sendMessage(MainActivity.STATUS_COUNTING);
                    Log.d(TAG, "Service time: " + time);
                    TimeUnit.SECONDS.sleep(1);
                } catch (Exception ex){
                    Log.e(TAG, "Interrupted!");
                }
            }
        }
    }
}
