package ru.startandroid.timetracker;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.startandroid.timetracker.db.TaskContract;
import ru.startandroid.timetracker.db.TaskDbHelper;

public class MainActivity extends AppCompatActivity {
    public static final int COUNTER_REQUEST = 0;

    public static final int STATUS_FINISHED = 27;
    public static final int STATUS_COUNTING = 28;

    private TaskDbHelper mHelper;
    private ListView mTaskListView;
    private TaskArrayAdapter mAdapter;
    //private ArrayList<Task> taskList;
    private TaskList taskList;
    private String currTask = "";

    private PendingIntent pi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    //        .setAction("Action", null).show();

                    addNewTask();
                }
            });
        }

        mHelper = new TaskDbHelper(this);
        mTaskListView = (ListView) findViewById(R.id.list_todo);

        loadDB();
        updateUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateUI(){

        findViewById(R.id.empty).setVisibility(View.INVISIBLE);

        if(mAdapter == null) {
            mAdapter = new TaskArrayAdapter(this, R.layout.item_todo, new ArrayList<Task>(taskList.getList()));
            mTaskListView.setAdapter(mAdapter);
        } else {
            mAdapter.clear();
            mAdapter.addAll(new ArrayList<Task>(taskList.getList()));
            mAdapter.notifyDataSetChanged();
        }


        if (taskList.getList().size() == 0){
            findViewById(R.id.empty).setVisibility(View.VISIBLE);
        }
    }

    private void loadDB(){
        taskList = new TaskList();
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.query(TaskContract.TaskEntry.TABLE,
                new String[]{
                        TaskContract.TaskEntry._ID,
                        TaskContract.TaskEntry.COL_TASK_TITLE,
                        TaskContract.TaskEntry.COL_TASK_TIME},
                null, null, null, null, null);

        while (cursor.moveToNext()){
            int idTitle = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TITLE);
            int idTime = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TIME);
            taskList.getList().add(new Task(cursor.getString(idTitle), cursor.getLong(idTime)));
        }

        cursor.close();
        db.close();
    }

    public void addNewTask(){
        //final EditText taskEditText = new EditText(this);

        final AutoCompleteTextView taskEditText = new AutoCompleteTextView(this);
        ArrayList<String> taskNames = new ArrayList();
        for (Task t : taskList.getList()){
            taskNames.add(t.name);
        }

        ArrayAdapter hintAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, taskNames.toArray());


        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Add a new task")
                .setMessage("Input the name of new task:")
                .setView(taskEditText)
                .setPositiveButton("Add", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        String task = String.valueOf(taskEditText.getText());
                        SQLiteDatabase db = mHelper.getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(TaskContract.TaskEntry.COL_TASK_TITLE, task);
                        values.put(TaskContract.TaskEntry.COL_TASK_TIME, 0);
                        db.insertWithOnConflict(TaskContract.TaskEntry.TABLE,
                                null,
                                values,
                                SQLiteDatabase.CONFLICT_REPLACE);
                        db.close();

                        taskList.getList().add(new Task(task, 0));

                        updateUI();
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    public void deleteTask(View view){
        View parent = (View)view.getParent();
        TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());
        Log.d("Main Acitvity", "deletint task: " + task);

        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.delete(TaskContract.TaskEntry.TABLE, TaskContract.TaskEntry.COL_TASK_TITLE + " = ?",
                new String[] {task});
        db.close();

        taskList.removeByName(task);

        updateUI();
    }

    public void updateTask(Task task){
        ContentValues cv = new ContentValues();

        cv.put(TaskContract.TaskEntry.COL_TASK_TITLE, task.name);
        cv.put(TaskContract.TaskEntry.COL_TASK_TIME, task.timeSpent);

        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.update(TaskContract.TaskEntry.TABLE, cv, TaskContract.TaskEntry.COL_TASK_TITLE + " = ?", new String[]{ task.name });
        db.close();
    }

    public void executeTask(View view){
        View parent = (View)view.getParent();
        TextView taskTextView = (TextView)parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());

        if (!currTask.isEmpty()) {
            stopService(new Intent(this, Counter.class));
        }

        if (!currTask.equalsIgnoreCase(task)){
            pi = createPendingResult(COUNTER_REQUEST, new Intent(), 0);

            startService(new Intent(this, Counter.class)
                    .putExtra("time", taskList.getTaskByName(task).timeSpent)
                    .putExtra("name", task)
                    .putExtra("pendingIntent", pi)
            );
            currTask = task;
        } else {
            currTask = "";
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        long time = data.getLongExtra("time", 0);
        String name = data.getStringExtra("name");

        Task t = taskList.getTaskByName(name);
        if (t != null) {
            taskList.updateTimeByName(name, time);
            updateUI();

            if (resultCode == STATUS_FINISHED) {
                Log.d("Main Activity", "Saving task: " + t.name);
                updateTask(t);
            }
        }
    }

    private class TaskArrayAdapter extends ArrayAdapter<Task>{

        private ArrayList<Task> items;

        public TaskArrayAdapter(Context context, int textViewResourceId, ArrayList<Task> items){
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View v = convertView;
            if (v == null){
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.item_todo, null);
            }
            Task t = items.get(position);
            if (t != null){
                TextView titleText = (TextView)v.findViewById(R.id.task_title);
                TextView timeText = (TextView)v.findViewById(R.id.task_time);
                if (titleText != null){
                    titleText.setText(t.name);
                }
                if (timeText != null){
                    timeText.setText(t.getTimeSpentString());
                }
            }
            return v;
        }
    }
}
