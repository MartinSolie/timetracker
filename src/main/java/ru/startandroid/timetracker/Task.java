package ru.startandroid.timetracker;

/**
 * Created by martin on 6/1/16.
 */
public class Task {
    public String name;
    public long timeSpent;

    Task(String name){
        this.name = name;
        this.timeSpent = 0;
    }

    Task(String name, long timeSpent){
        this.name = name;
        this.timeSpent = timeSpent;
    }

    public String getTimeSpentString(){
        long tmpTime = timeSpent;

        String timeString = "";
        if (tmpTime / 3600 < 10) timeString += "0";
        timeString += tmpTime / 3600;
        tmpTime %= 3600;
        timeString+=":";
        if (tmpTime / 60 < 10) timeString += "0";
        timeString += tmpTime / 60;
        tmpTime %= 60;
        timeString += ":";
        if (tmpTime < 10) timeString += "0";
        timeString += tmpTime;
        return timeString;
    }
}
